package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"

	"github.com/tidwall/buntdb"
)

const (
	Green  = "\u001b[32m"
	Red    = "\u001b[31m"
	Yellow = "\u001b[33m"
	White  = "\u001b[0m"
)

var (
	db	     *buntdb.DB
	ct       string
	err		 error
	id		 int
	level	 int
	inputId  string
	priority string
	task	 string
)

func Tasks() {
	fmt.Printf("%vPAJA%v list:\n\n", Yellow, White)
	fmt.Println("ID:\tTask:")

	// Return tasks sorted by priority.
	db.View(func(tx *buntdb.Tx) error {
		tx.Ascend("", func(key, value string) bool {
			// Maldito Go.
			mR := regexp.MustCompile(`^[1]:`)
			mY := regexp.MustCompile(`^[2]:`)
			mG := regexp.MustCompile(`^[3]:`)

			if mR.FindString(value) == "1:" {
				ct  = mR.ReplaceAllString(value, Red)
			} else if mY.FindString(value) == "2:"{
				ct  = mY.ReplaceAllString(value, Yellow)
			} else {
				ct  = mG.ReplaceAllString(value, Green)
			}

			fmt.Printf("%v%s\t%s\n", White, key, ct)
			return true
		})
		return nil
	})
}

func NewTask() {
	// Read the new task.
	// And save task and priortiy variable.
	inTask     := bufio.NewScanner(os.Stdin)
	inPriority := bufio.NewScanner(os.Stdin)

	fmt.Println("Write a task:")
	for {
		inTask.Scan()
		task = inTask.Text()

		if len(task) != 0 {
			fmt.Println("Priority level (1 - 3)")
			for  {
				inPriority.Scan()
				priority = inPriority.Text()
			 	isPriority, _ := regexp.MatchString("(^|\\s)[123](\\s|$)", priority)

				if len(priority) != 0 && isPriority {
					break
				} else {
					fmt.Println("Priority level (1 - 3)")
				}
			}
			break
		} else {
			fmt.Println("Write a task:")
		}
	}

	// The worst way to sort the priorty.
	switch priority {
    case "1":
		task = "1:" + task
	case "2":
		task = "2:" + task
	case "3":
		task = "3:" + task
	}
}

func Add() {
	NewTask() // Get the new task.

	// Return the last key/ID.
	db.View(func(tx *buntdb.Tx) error {
		tx.Descend("", func(key, value string) bool {
			id, _ = strconv.Atoi(key)
			id += 1
			return false
		})
		return nil
	})

	// Add the new task.
	db.Update(func(tx *buntdb.Tx) error {
		_, _, err := tx.Set(strconv.Itoa(id), task, nil)
		if err != nil {
			return err
		}

		fmt.Println(Green, "\nNew task added")
		return nil
	})
}

// Get ID from standard input.
func Id() {
	fmt.Println(White, "\nInsert task ID:")
	inId := bufio.NewScanner(os.Stdin)
	for {
		inId.Scan()
		inputId = inId.Text()
		isId, _ := regexp.MatchString("[0-9]", inputId)

		if len(inputId) != 0 && isId {
			break
		} else {
			fmt.Println("Invalid ID. Try again:")
		}
	}
}

// Edit a taks passing an ID.
func Edit() {
	Tasks() // Return tasks.
	Id() // Save ID.
	NewTask() // Get the new task.

	db.Update(func(tx *buntdb.Tx) error {
		tx.Set(inputId, task, nil)

		fmt.Printf("\n%vTask %v has been modified\n", Green, inputId)
		return nil
	})

	Tasks()
}

func Remove() {
	Tasks()
	Id()

	// Update PAJA's list.
	db.Update(func(tx *buntdb.Tx) error {
		tx.Delete(inputId)
		fmt.Printf("The task has been deleted.")

		return nil
	})
}

func Help() {
	fmt.Println("Usage:")
	fmt.Println("paja [add | -a] [edit | -e] [rem | -r] [help | -h]\n")
	fmt.Println("paja	  Show the PAJA's list.\n")
	fmt.Println("add  -a:    Add a new task with priority number (1 - 3).")
	fmt.Println("edit -e:    Edit a task by ID.")
	fmt.Println("rem  -r:    Delete a task by ID.")
	fmt.Println("help -h:    Show this help message.")
}

func main() {
	// Open or create the database file.
	// Edit for custom path, e.g. "new/paja/directory/paja.db".
	db, err = buntdb.Open("paja.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Command arguments.
	if len(os.Args) < 2 {
		Tasks()
	} else if len(os.Args) > 1 {
		switch os.Args[1] {
		case "add", "-a":
			Add()
		case "edit", "-e":
			Edit()
		case "rem", "-r":
			Remove()
		case "help", "-h":
			Help()
		default:
			Help()
		}
	}
}

